# Sokoban



## INTRODUCCION

Sokoban es un juego de origen japonés creado por Imabayashi Hiroyuki en 1980. El objetivo del juego es mover unas cajas a su lugar correspondiente, empujandolas de una en una.

Este proyecto consiste en crear una versión funcional de Unity del clásico juego. Se incluyen tanto una colección de niveles de prueba como un editor de niveles para crear nuevos.

## MECANICAS

Sokoban trata de colocar cajas en partes concretas del escenario. Estos son los movimientos permitidos:
    - Las cajas se pueden empujar a una casilla adyacente.
    - No se puede tirar de las cajas.

Este juego es simple pero esto hace que los puzles que se generan lleguen a un nivel de profundidad impresionante. 


## EDITOR DE NIVELES

Además de los niveles creados, también está la posibilidad de crear nuestros niveles personalizados con el editor.


## SCREENSHOTS

![Primer nivel][def]

[def]: images/level.png

![Ejemplo del editor de niveles][def2]

[def2]: images/editor.png

![Dimensiones del tablero][def3]

[def3]: images/dimensions.png

![Resultado][def4]

[def4]: images/editor2.png