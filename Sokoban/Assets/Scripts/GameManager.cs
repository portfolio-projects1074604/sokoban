using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int level_index = 1;
    private static GameManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else Destroy(gameObject);
    }

    public void LoadNextLevel()
    {
        level_index++;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public int GetLevelIndex()
    {
        return level_index;
    }

    public void ResetIndex()
    {
        level_index = 1;
    }
}
