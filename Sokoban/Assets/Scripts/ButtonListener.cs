using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonListener : MonoBehaviour
{
    public Button reset_button;
    public GameManager gm;
    private void Awake()
    {
        reset_button = gameObject.GetComponent<Button>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        reset_button.onClick.AddListener(gm.ReloadLevel);
    }
}
