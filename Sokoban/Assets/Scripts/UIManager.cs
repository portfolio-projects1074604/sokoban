using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject sizeSection;
    public GameObject editorSection;
    public Button make_level;

    private bool canEdit = true;

    public void HideSize()
    {
        sizeSection.SetActive(false);
    }

    public void HideEditor()
    {
        editorSection.SetActive(false);
        canEdit = true;
    }

    public void ShowEditor()
    {
        editorSection.SetActive(true);       
    }

    public void ShowLevelMaker()
    {
        make_level.gameObject.SetActive(true);
    }

    public void ActivateOptions()
    {
        canEdit = false;
        ShowEditor();
    }

    public bool CanEdit()
    {
        return canEdit;
    }

}
