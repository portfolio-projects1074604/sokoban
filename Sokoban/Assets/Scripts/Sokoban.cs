using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sokoban : MonoBehaviour
{
    public GameManager gm;

    public int[,] levelData;

    public Vector2 middleOffset;

    private int invalidTile = -1;
    private int groundTile = 1;
    private int heroTile = 2;
    private int ballTile = 3;
    private int destinationTile = 0;
    private int heroOnDestinationTile = -2;
    private int ballOnDestinationTile = -3;
    private bool gameover = false;

    private int rows;
    private int columns;
    private int tileSize = 50;
    private int ballCount;
    public Sprite tileSprite;
    public Sprite heroSprite;
    public Sprite ballSprite;

    GameObject hero;

    Dictionary<GameObject, Vector2> occupants;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        occupants = new Dictionary<GameObject, Vector2>();
        middleOffset = new Vector2();
        ParseLevel();
        CreateLevel();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameover)
        {
            return;
        }
        ApplyUserInput();
    }

    public void ParseLevel()
    {
        TextAsset textFile = Resources.Load("Level " + gm.GetLevelIndex()) as TextAsset;
        if (textFile == null)
        {
            textFile = Resources.Load("Level 1") as TextAsset;
            gm.ResetIndex();
        }
        string[] lines = textFile.text.Split(new[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        string[] nums = lines[0].Split(new[] { ',' });
        rows = lines.Length;
        columns = nums.Length;
        levelData = new int[rows, columns];
        for(int i = 0; i < rows; i++)
        {
            string st = lines[i];
            nums = st.Split(new[] { ',' });
            for (int j = 0; j < columns; j++)
            {
                int val;
                if (int.TryParse(nums[j], out val))
                {
                    levelData[i, j] = val;
                }
                else
                {
                    levelData[i, j] = invalidTile;
                }
            }
        }
    }

    public void CreateLevel()
    {
        GameObject tile;
        SpriteRenderer sr;
        GameObject ball;

        middleOffset.x = columns * tileSize * 0.5f - tileSize * 0.5f;
        middleOffset.y = rows * tileSize * 0.5f - tileSize * 0.5f;

        int destinationCount = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                int val = levelData[i, j];
                if (val != invalidTile)
                {
                    tile = new GameObject("Tile" + i.ToString() + "_" + j.ToString());
                    tile.transform.localScale = Vector2.one * (tileSize - 1);
                    sr = tile.AddComponent<SpriteRenderer>();
                    sr.sprite = tileSprite;
                    tile.transform.position = GetScreenPointFromLevelIndices(i, j);
                    if (val == destinationTile)
                    {
                        sr.color = Color.blue;
                        destinationCount++;
                    }
                    else
                    {
                        if (val == heroTile || val == heroOnDestinationTile)
                        {
                            if (val == heroOnDestinationTile)
                            {
                                sr.color = Color.blue;
                            }
                            hero = new GameObject("hero");
                            hero.transform.localScale = Vector2.one * (tileSize - 1);
                            sr = hero.AddComponent<SpriteRenderer>();
                            sr.sprite = heroSprite;
                            sr.sortingOrder = 1;
                            sr.color = Color.red;
                            hero.transform.position = GetScreenPointFromLevelIndices(i, j);
                            occupants.Add(hero, new Vector2(i, j));
                        }
                        else if (val == ballTile || val == ballOnDestinationTile)
                        {
                            if (val == ballOnDestinationTile)
                            {
                                sr.color = Color.blue;
                            }
                            ballCount++;
                            ball = new GameObject("ball" + ballCount.ToString());
                            ball.transform.localScale = Vector2.one * (tileSize - 1);
                            sr = ball.AddComponent<SpriteRenderer>();
                            sr.sprite = ballSprite;
                            sr.sortingOrder = 1;
                            sr.color = Color.black;
                            ball.transform.position = GetScreenPointFromLevelIndices(i, j);
                            occupants.Add(ball, new Vector2(i, j));
                        }
                    }
                }
            }
        }
        if (ballCount > destinationCount) 
        {
            Debug.LogError("More balls than destiantions");
        }
    }

    private GameObject GetOccupantAtPosition(Vector2 heroPos)
    {
        GameObject ball;
        foreach (KeyValuePair<GameObject, Vector2> pair in occupants)
        {
            if (pair.Value == heroPos)
            {
                ball = pair.Key;
                return ball;
            }
        }
        return null;
    }

    public Vector2 GetScreenPointFromLevelIndices(int row, int column)
    {
        return new Vector2(column * tileSize - middleOffset.x, row * -tileSize + middleOffset.y); 
    }

    private bool IsOccupied(Vector2 objPos)
    {
        return (levelData[(int)objPos.x, (int)objPos.y] == ballTile || levelData[(int)objPos.x, (int)objPos.y] == ballOnDestinationTile);
    }

    private bool IsValidPosition(Vector2 objPos)
    {
        if (objPos.x > -1 && objPos.x < rows && objPos.y > -1 && objPos.y < columns)
        {
            return levelData[(int)objPos.x, (int)objPos.y] != invalidTile;
        }
        else return false;
    }

    private void ApplyUserInput()
    {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            TryMoveHero(0);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            TryMoveHero(1);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            TryMoveHero(2);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            TryMoveHero(3);
        }
    }

    private void TryMoveHero(int direction)
    {
        Vector2 heroPos;
        Vector2 oldHeroPos;
        Vector2 nextPos;
        occupants.TryGetValue(hero, out oldHeroPos);
        heroPos = GetNextPositionAlong(oldHeroPos, direction);//find the next array position in given direction

        if (IsValidPosition(heroPos))
        {//check if it is a valid position & falls inside the level array
            if (!IsOccupied(heroPos))
            {//check if it is occupied by a ball
             //move hero
                RemoveOccupant(oldHeroPos);//reset old level data at old position
                hero.transform.position = GetScreenPointFromLevelIndices((int)heroPos.x, (int)heroPos.y);
                occupants[hero] = heroPos;
                if (levelData[(int)heroPos.x, (int)heroPos.y] == groundTile)
                {//moving onto a ground tile
                    levelData[(int)heroPos.x, (int)heroPos.y] = heroTile;
                }
                else if (levelData[(int)heroPos.x, (int)heroPos.y] == destinationTile)
                {//moving onto a destination tile
                    levelData[(int)heroPos.x, (int)heroPos.y] = heroOnDestinationTile;
                }
            }
            else
            {
                //we have a ball next to hero, check if it is empty on the other side of the ball
                nextPos = GetNextPositionAlong(heroPos, direction);
                if (IsValidPosition(nextPos))
                {
                    if (!IsOccupied(nextPos))
                    {//we found empty neighbor, so we need to move both ball & hero
                        GameObject ball = GetOccupantAtPosition(heroPos);//find the ball at this position
                        if (ball == null) Debug.Log("no ball");
                        RemoveOccupant(heroPos);//ball should be moved first before moving the hero
                        ball.transform.position = GetScreenPointFromLevelIndices((int)nextPos.x, (int)nextPos.y);
                        occupants[ball] = nextPos;
                        if (levelData[(int)nextPos.x, (int)nextPos.y] == groundTile)
                        {
                            levelData[(int)nextPos.x, (int)nextPos.y] = ballTile;
                        }
                        else if (levelData[(int)nextPos.x, (int)nextPos.y] == destinationTile)
                        {
                            levelData[(int)nextPos.x, (int)nextPos.y] = ballOnDestinationTile;
                        }
                        RemoveOccupant(oldHeroPos);//now move hero
                        hero.transform.position = GetScreenPointFromLevelIndices((int)heroPos.x, (int)heroPos.y);
                        occupants[hero] = heroPos;
                        if (levelData[(int)heroPos.x, (int)heroPos.y] == groundTile)
                        {
                            levelData[(int)heroPos.x, (int)heroPos.y] = heroTile;
                        }
                        else if (levelData[(int)heroPos.x, (int)heroPos.y] == destinationTile)
                        {
                            levelData[(int)heroPos.x, (int)heroPos.y] = heroOnDestinationTile;
                        }
                    }
                }
            }
            CheckCompletion();//check if all balls have reached destinations
        }
    }

    private Vector2 GetNextPositionAlong(Vector2 objPos, int direction)
    {
        switch (direction)
        {
            case 0:
                objPos.x -= 1;//up
                break;
            case 1:
                objPos.y += 1;//right
                break;
            case 2:
                objPos.x += 1;//down
                break;
            case 3:
                objPos.y -= 1;//left
                break;
        }
        return objPos;
    }

    private void RemoveOccupant(Vector2 objPos)
    {
        if (levelData[(int)objPos.x, (int)objPos.y] == heroTile || levelData[(int)objPos.x, (int)objPos.y] == ballTile)
        {
            levelData[(int)objPos.x, (int)objPos.y] = groundTile;//ball moving from ground tile
        }
        else if (levelData[(int)objPos.x, (int)objPos.y] == heroOnDestinationTile)
        {
            levelData[(int)objPos.x, (int)objPos.y] = destinationTile;//hero moving from destination tile
        }
        else if (levelData[(int)objPos.x, (int)objPos.y] == ballOnDestinationTile)
        {
            levelData[(int)objPos.x, (int)objPos.y] = destinationTile;//ball moving from destination tile
        }
    }

    private void CheckCompletion()
    {
        int ballsOnDestination = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (levelData[i, j] == ballOnDestinationTile)
                {
                    ballsOnDestination++;
                }
            }
        }
        if (ballsOnDestination == ballCount)
        {
            Debug.Log("level complete");
            gameover = true;
            gm.LoadNextLevel();
        }
    }
}
