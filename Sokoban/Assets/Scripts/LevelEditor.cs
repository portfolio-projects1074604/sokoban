using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LevelEditor : MonoBehaviour
{
    public UIManager manager;

    public int[,] level;
    public int tileSize = 5;

    private const int invalidTile = -1;
    private const int groundTile = 1;
    private const int heroTile = 2;
    private const int ballTile = 3;
    private const int destinationTile = 0;
    private const int heroOnDestinationTile = -2;
    private const int ballOnDestinationTile = -3;

    private int rows = 0;
    private int columns = 0;
    private Vector2 middleOffset;

    private int ballCount = 0;
    private int destinationCount = 0;
    public Sprite tileSprite;
    public Sprite heroSprite;
    public Sprite ballSprite;

    GameObject actualTile;

    private Dictionary<Vector2, GameObject> occupants;

    private bool level_generated = false;

    // Start is called before the first frame update
    void Start()
    {
        middleOffset = new Vector2();
        occupants = new Dictionary<Vector2, GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rows != 0 && columns != 0)
        {  
            manager.HideSize();
            manager.ShowLevelMaker();
            if (!level_generated)
            {
                level = new int[rows, columns];
                CreateLevel();
                level_generated = true;
                
            }
            if (Input.GetMouseButtonDown(0) && manager.CanEdit())
            {
                Debug.Log(manager.CanEdit());
                
                RaycastHit2D hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero);
                if (hit.collider != null)
                {
                    Debug.Log(hit.collider.gameObject.name);
                    actualTile = hit.collider.gameObject;
                    manager.ActivateOptions();
                }
            }
        }      
    }

    

    public void ChangeTile(int type)
    {
        float[] index = GetIndicesFromScreenPoint(actualTile.transform.position.x, actualTile.transform.position.y);
        int i = (int)index[0];
        int j = (int)index[1];
        Debug.Log(i);
        Debug.Log(j);
        
        switch (type)
        {
            case groundTile:
                if (occupants.ContainsKey(new Vector2(index[0], index[1])))
                {
                    occupants.TryGetValue(new Vector2(index[0], index[1]), out GameObject value);
                    if (value.GetComponent<SpriteRenderer>().sprite == ballSprite)
                    {
                        ballCount--;
                    }
                    occupants.Remove(new Vector2(index[0], index[1]));
                    Destroy(value);
                }
                if (actualTile.GetComponent<SpriteRenderer>().color == Color.blue)
                {
                    destinationCount--;
                }
                actualTile.GetComponent<SpriteRenderer>().sprite = tileSprite;
                actualTile.GetComponent<SpriteRenderer>().color = Color.white;
                level[i, j] = groundTile;
                break;
            case heroTile:
                GameObject hero = new GameObject("Hero " + index[0] + "_" + index[1]);
                hero.transform.localScale = Vector2.one * (tileSize - 1);
                SpriteRenderer sr = hero.AddComponent<SpriteRenderer>();
                sr.sprite = heroSprite;
                sr.sortingOrder = 1;
                sr.color = Color.red;
                hero.transform.position = actualTile.transform.position;
                occupants.Add(new Vector2(index[0], index[1]), hero);
                if (actualTile.GetComponent<SpriteRenderer>().color == Color.blue)
                {
                    level[i, j] = heroOnDestinationTile;
                }
                else level[i, j] = heroTile;
                break;
            case ballTile:
                GameObject ball = new GameObject("Ball " + index[0] + "_" + index[1]);
                ball.transform.localScale = Vector2.one * (tileSize - 1);
                sr = ball.AddComponent<SpriteRenderer>();
                sr.sprite = ballSprite;
                sr.sortingOrder = 1;
                sr.color = Color.black;
                ball.transform.position = actualTile.transform.position;
                occupants.Add(new Vector2(index[0], index[1]), ball);
                ballCount++;
                if (actualTile.GetComponent<SpriteRenderer>().color == Color.blue)
                {
                    level[i, j] = ballOnDestinationTile;
                }
                else level[i, j] = ballTile;
                break;
            case destinationTile:
                actualTile.GetComponent<SpriteRenderer>().sprite = tileSprite;
                actualTile.GetComponent<SpriteRenderer>().color = Color.blue;
                destinationCount++;
                level[i, j] = destinationTile;
                Debug.Log(level[i, j]);
                break;
            case invalidTile:
                if (occupants.ContainsKey(new Vector2(index[0], index[1])))
                {
                    occupants.TryGetValue(new Vector2(index[0], index[1]), out GameObject value);
                    if (value.GetComponent<SpriteRenderer>().sprite == ballSprite)
                    {
                        ballCount--;
                    }
                    occupants.Remove(new Vector2(index[0], index[1]));
                    Destroy(value);
                }
                if (actualTile.GetComponent<SpriteRenderer>().color == Color.blue)
                {
                    destinationCount--;
                }
                actualTile.GetComponent<SpriteRenderer>().color = Color.clear;
                level[i, j] = invalidTile;
                break;
            default: break;
        }
        Debug.Log(level[i, j]);
    }

    public void GetRow(string row)
    {
        rows = Int32.Parse(row);
    }

    public void GetCol(string col)
    {
        columns = Int32.Parse(col);
    }

    private void CreateLevel()
    {
        GameObject tile;
        SpriteRenderer sr;

        middleOffset.x = columns * tileSize * 0.5f - tileSize * 0.5f;
        middleOffset.y = rows * tileSize * 0.5f - tileSize * 0.5f;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                tile = new GameObject("Tile" + i.ToString() + "_" + j.ToString());
                tile.transform.localScale = Vector2.one * (tileSize - 1);
                tile.AddComponent<BoxCollider2D>();
                sr = tile.AddComponent<SpriteRenderer>();
                sr.sprite = tileSprite;
                tile.transform.position = GetScreenPointFromLevelIndices(i, j);
                level[i, j] = groundTile;
            }
        }
    }

    //M�todo para obtener la posicion del objeto segun sus indices en la matriz
    public Vector2 GetScreenPointFromLevelIndices(int row, int column)
    {
        return new Vector2(column * tileSize - middleOffset.x, row * -tileSize + middleOffset.y);
    }
    
    //M�todo para obtener los indices en la matriz del objeto segun su posicion
    public float[] GetIndicesFromScreenPoint(float x, float y)
    {
        float[] indices = { (y - middleOffset.y) / -tileSize, (x + middleOffset.x) / tileSize };
        return indices;
    }
    public void LevelToFile()
    {
        int x = 0;
        var fileName = "Level " + x + ".txt";

        do
        {
            x++;
            fileName = "Level " + x + ".txt";
        } while (File.Exists("Assets/Resources/" + fileName));
        Debug.Log("Salgo del bucle");
        var file = File.CreateText("Assets/Resources/" + fileName);
        
        for(int i = 0; i < rows; i++)
        {
            Debug.Log("Primer bucle");
            for(int j = 0; j < columns; j++)
            {
                Debug.Log("Segundo bucle");
                if (j == columns- 1)
                {
                    file.WriteLine(level[i, j]);
                }
                else file.Write(level[i, j] + ",");
            }
        }
        file.Close();
    }
}
